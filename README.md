This is a demo for Active8 multi environments in Vercel

* Production URL: [active8-prod.vercel.app/](https://active8-prod.vercel.app/)
* Staging URL: [active8-staging.vercel.app/](https://active8-staging.vercel.app/)

Please note that **Active8 Environment:  production** and **Active8 Environment: staging** comes from `process.env.APP_ENVIRONMENT` in `src/app/page.tsx` and `src/app/layout.tsx`

You can clone this project and make a push in `main` or `staging` branches. The differences will only be reflected in their respective environments, thanks to the Gitlab CI that runs for each environment.