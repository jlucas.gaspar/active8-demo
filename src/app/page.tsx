import styles from "./page.module.css";

export default function Home() {
  return (
    <main className={styles.main}>
      <div className={styles.description}>
        <p>
          Active8 Environment: &nbsp;
          <code className={styles.code}>
            {process.env.APP_ENVIRONMENT}
          </code>
        </p>
      </div>
    </main>
  );
}
